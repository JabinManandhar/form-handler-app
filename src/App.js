import React, { Component } from 'react'

export default class App extends Component {
  state = {
    firstName: '',
    submissionData: false,
    lastName: ''
  }

  handleChange(event) {
    let { name, value } = event.target;

    if (name === 'firstName') {
      this.setState({
        firstName: value
      })
    }
    else if (name === 'lastName') {
      this.setState({
        lastName: value
      })
    }
    else if (name === 'phoneNo') {
      this.setState({
        phoneNo: value
      })
    }
    else if (name === 'email') {
      this.setState({
        email: value
      })
    }
    else if (name === 'address') {
      this.setState({
        address: value
      })
    }

  }
  handleSubmit(event) {
    this.setState({
      submissionData: true
    })
  }

  render() {
    return (
      <div>
        <h1>Form Handling</h1>
        <div>
          <label>First Name:</label>
          <input
            type="text"
            name='firstName'
            value={this.state.firstName}
            onChange={(event) => { this.handleChange(event) }} />
        </div>
        <div>
          <label>Last Name:</label>
          <input
            type="text"
            name='lastName'
            value={this.state.lastName}
            onChange={(event) => { this.handleChange(event) }} /></div>
        <div>
          <label>Phone number:</label>
          <input
            type="text"
            name='phoneNo'
            value={this.state.phoneNo}
            onChange={(event) => { this.handleChange(event) }} /></div>
        <div>
          <label>Email:</label>
          <input
            type="text"
            name='email'
            value={this.state.email}
            onChange={(event) => { this.handleChange(event) }} /></div>
        <div>
          <label>Address:</label>
          <input
            type="text"
            name='address'
            value={this.state.address}
            onChange={(event) => { this.handleChange(event) }} /></div>
        <div> <button onClick={(event => { this.handleSubmit({ submissionData: true }) })}>Submit</button></div>

        {
          this.state.submissionData === true &&
          <div>
            <p>Your Name is:{this.state.firstName}{this.state.lastName}</p>
            <p>Your Phone Number is:{this.state.phoneNo}</p>
            <p>Your Email is:{this.state.email}</p>
            <p>Your Address is:{this.state.address}</p>
          </div>
        }
      </div>
    )
  }
}

